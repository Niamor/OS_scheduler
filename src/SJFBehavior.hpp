#ifndef SJF_BEHAVIOR_HPP
#define SJF_BEHAVIOR_HPP

#define       FIFO_RATE     99

#include "priorityProcessQueue.hpp"
#include "abstractSchedulerBehavior.hpp"

class SJFBehavior : public AbstractSchedulerBehavior
{
private:
  typedef AbstractSchedulerBehavior super;

public:
  SJFBehavior();
  SJFBehavior(ProcessQueue *q);
  void init (ProcessQueue *q);
  void addProcess(Process *p);
  ProcessExecuteData *routine();
};

#endif
