#include "processQueue.hpp"

ProcessQueue::ProcessQueue () : super()
{
}

int ProcessQueue::getDate ()
{
  return ((ProcessList*)getFirst())->getDate();
}

void ProcessQueue::addProcess (Process *p, int d)
{
  ProcessList *elt = new ProcessList(p, d);
  if (getFirst() == NULL) {
    super::add(elt);
  }
  else if (d < getDate()) {
    elt->setNext((ProcessList*)getFirst());
    head = elt;
    size++;
  }
  else {
    ProcessList *prev = (ProcessList*)getFirst();
    ProcessList *curr = prev->getNext();
    while (curr != NULL && d >= curr->getDate()) {
      prev = curr;
      curr = curr->getNext();
    }
    elt->setNext(curr);
    prev->setNext(elt);
    if (curr == NULL) {
      tail = elt;
    }
    size++;
  }
}

Process *ProcessQueue::getProcess ()
{
  return ((ProcessList*)getFirst())->getProcess();
}
