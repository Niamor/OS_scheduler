#ifndef LOGGER_H
#define LOGGER_H

#include "main.hpp"
#include <fstream>

enum Dev_Log_Level
{
  LOG_DEBUG = 0,
  LOG_TRACE = 1,
  LOG_INFO = 2,
  LOG_WARN = 3,
  LOG_ERROR = 4
};

class Logger_policy
{
public:
  virtual void open(const char *name) = 0;
  virtual void close() = 0;
  virtual void log(Dev_Log_Level level, const std::string message) = 0;

protected:
  virtual void write(const std::string message) = 0;
  std::string levelToString(Dev_Log_Level level) const;
};

class Logger : public Logger_policy
{
public:
  static Logger& getInstance();
  void lock();
  void unlock();
  void open(const char *name);
  void close();
  void log(Dev_Log_Level level, const std::string message);

private:
  Logger(); //un constructeur
  ~Logger(); // destructeur
  Logger& operator= (const Logger&){} // EN C++ on a 5 constructeurs
  Logger (const Logger&) {} // Un constructeur
  static Logger INSTANCE; //Instanciation de l'objet (les constructeurs soient privés, objet est comme attribut lui meme, méthode qui renvoie lui meme)

  void write(const std::string message); // std::string
  bool isLocked;
  std::ofstream output; //std::offtsream => existence d'un type. ofstream vient de std
};

#endif
