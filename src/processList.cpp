#include "processList.hpp"

ProcessList::ProcessList (Process *p, int d) : super((void*)p), date(d)
{
  next = NULL;
}

Process *ProcessList::getProcess ()
{
  return (Process*)super::getValue();
}

int ProcessList::getDate ()
{
  return date;
}

void ProcessList::setDate (int d)
{
  date = d;
}

ProcessList *ProcessList::getNext ()
{
  return (ProcessList*) super::getNext();
}

void ProcessList::setNext (ProcessList *l)
{
  super::setNext((AbstractList*) l);
}
