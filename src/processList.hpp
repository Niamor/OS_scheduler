#ifndef PROCESS_LIST_HPP
#define PROCESS_LIST_HPP

#include "abstractList.hpp"
#include "process.hpp"

class ProcessList : public AbstractList
{
private:
    typedef AbstractList super;
    int date;

public:
    ProcessList(Process *p, int d);
    int getDate();
    void setDate(int d);
    void setNext(ProcessList *l);
    ProcessList *getNext();
    Process *getProcess();
};

#endif
