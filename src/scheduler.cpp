#include "scheduler.hpp"

Scheduler::Scheduler()
{
  queue = NULL;
}

void Scheduler::init(ProcessQueue **q)
{
  delete(queue);
  queue = q[1];
  if (q[0] != NULL && !q[0]->isEmpty()) {
    if (currentBehavior != NULL) {
      currentBehavior->setQueue(q[0]);
    }
  }
}


void Scheduler::setCurrentBehavior (AbstractSchedulerBehavior *behavior)
{
  currentBehavior = behavior;
  if (behavior != NULL) {
    if (queue != NULL) {
      while (!queue->isEmpty() && ((ProcessList*)queue->getFirst())->getDate() <= currentBehavior->getIterations()) {
        currentBehavior->add(queue->pop());
      }
    }
  }
}

int Scheduler::processes ()
{
  if (queue != NULL) {
    while (!queue->isEmpty() && ((ProcessList*)queue->getFirst())->getDate() <= currentBehavior->getIterations())
      currentBehavior->add(queue->pop());
  }
  return (currentBehavior == NULL) ? -1 : currentBehavior->routine();
}
