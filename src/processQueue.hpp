#ifndef PROCESS_QUEUE_HPP
#define PROCESS_QUEUE_HPP

#include "abstractQueue.hpp"
#include "processList.hpp"

class ProcessQueue : public AbstractQueue
{
private:
  typedef AbstractQueue super;

public:
  ProcessQueue();
  void addProcess(Process *p, int d);
  Process *getProcess();
  int getDate();
};

#endif
