#include "taskList.hpp"

TaskList::TaskList (Task *t) : super((void*)t)
{
//  next = NULL;
}

Task *TaskList::getTask ()
{
  return (Task*)super::getValue();
}


TaskList *TaskList::getNext ()
{
  return (TaskList*) super::getNext();
}

void TaskList::setNext (TaskList *l)
{
  super::setNext((AbstractList*) l);
}
