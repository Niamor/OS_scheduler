#ifndef PROCESS_HPP
#define PROCESS_HPP

#include "taskQueue.hpp"
#include "processExecuteData.hpp"

class Process
{
private:
  //current state
  State state;
  //list of tasks, pop when done
  TaskQueue *tasks;
  //timestamp of process arrival and id
  const int time, pid;
  int totalTime, remainingTime;
  //incremented each iteration
  int elapsed;

public:
  Process(int pid, int start);
  ProcessExecuteData *execute(int time, int duration);
  void addTask(bool IO, int duration);
  void setState(State s);
  State getState();
  int getPID();
  int getElapsed();
  int getTotalTime();
  int getRemaingTime();
};

#endif
