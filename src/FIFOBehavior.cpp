#include "FIFOBehavior.hpp"
FIFOBehavior::FIFOBehavior () : super()
{
  queue = new ProcessQueue();
}

FIFOBehavior::FIFOBehavior (ProcessQueue *q) : super((AbstractQueue*)q)
{
}
/*
FIFOBehavior::FIFOBehavior () : super()
{
  Process *process;
  ProcessQueue *queue = new ProcessQueue();

  process = new Process(1, 0);
  process->addTask(false, 5);
  process->addTask(true, 3);
  process->addTask(false, 2);
  queue->addProcess(process, 0);

  process = new Process(2, 0);
  process->addTask(false, 5);
  process->addTask(true, 3);
  process->addTask(false, 2);
  queue->addProcess(process, 0);

  process = new Process(3, 0);
  process->addTask(false, 5);
  process->addTask(true, 3);
  process->addTask(false, 2);
  queue->addProcess(process, 0);

  process = new Process(4, 0);
  process->addTask(false, 5);
  process->addTask(true, 3);
  process->addTask(false, 2);
  queue->addProcess(process, 0);

  process = new Process(5, 0);
  process->addTask(false, 5);
  process->addTask(true, 3);
  process->addTask(false, 2);
  queue->addProcess(process, 0);

  super::setQueue((AbstractQueue*)queue);
}
*/

void FIFOBehavior::addProcess (Process *p)
{
  ((ProcessQueue*)queue)->addProcess(p, iterations);
}

int FIFOBehavior::routine ()
{
  puts("test");
  ProcessList *tmp = (ProcessList*)queue->pop();
  char buffer[BUF_SIZE];
  sprintf(buffer, "PID %d : Duration %d : State %d\n", tmp->getProcess()->getPID(), tmp->getProcess()->getElapsed(), tmp->getProcess()->getState());
  puts(buffer);
  ProcessExecuteData *ped = tmp->getProcess()->execute(iterations - tmp->getDate(), FIFO_RATE);

  iterations += ped->getTotalTime();

  sprintf(buffer, "PID %d : Duration %d : State %d\n", tmp->getProcess()->getPID(), tmp->getProcess()->getElapsed(), tmp->getProcess()->getState());
  puts(buffer);
//  Logger::getInstance().log(LOG_INFO, buffer);

  if (tmp->getProcess()->getState() != STOPPED) {
    puts("back in queue");
    tmp->setDate (iterations);
    queue->add(tmp);
  }
  else {
    puts("stopped");
    delete(tmp);
  }

  if (queue->isEmpty()) {
    puts("empty queue");
    return -1;
  }

  return 0;
}
