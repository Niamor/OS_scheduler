#include "priorityProcessQueue.hpp"

PriorityProcessQueue::PriorityProcessQueue() : super()
{
}

ProcessList *PriorityProcessQueue::pop()
{
  ProcessList *process = ((ProcessList*)head);
  if (size <= 0 || process == NULL)
    return NULL;
  int min = process->getProcess()->getRemainingTime();
  ProcessList *prev = process, *curr = process->getNext();
  int it = size;
  while (curr != NULL && curr->getProcess() != NULL && size-- >= 0) {
    if (curr->getProcess()->getRemainingTime() < min) {
      min = curr->getProcess()->getRemainingTime();
      process = curr;
    }
    curr = curr->getNext();
  }
  curr = prev->getNext();
  if (process == head) {
    head = curr;
    return process;
  }
  while (curr != NULL) {
    if (curr->getProcess()->getPID() == process->getProcess()->getPID()) {
      prev->setNext(curr->getNext());
      if (curr->getNext() == NULL) {
        tail = prev;
      }
      break;
    }
    prev = curr;
    curr = curr->getNext();
  }
  return process;
}
