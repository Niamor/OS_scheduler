#include "abstractList.hpp"

AbstractList::AbstractList (void *v) : value(v)
{
  next = NULL;
}

AbstractList *AbstractList::getNext ()
{
  return next;
}

bool AbstractList::isEmpty ()
{
  return value == NULL;
}

void AbstractList::setNext (AbstractList *l)
{
  next = l;
}

void *AbstractList::getValue ()
{
  return value;
}
