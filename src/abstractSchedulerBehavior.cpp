#include "abstractSchedulerBehavior.hpp"

AbstractSchedulerBehavior::AbstractSchedulerBehavior ()
{
  iterations = 0;
}

AbstractSchedulerBehavior::AbstractSchedulerBehavior (AbstractQueue *q) : queue(q)
{
  iterations = 0;
}

void AbstractSchedulerBehavior::init (AbstractQueue *q)
{
  queue = q;
}

void AbstractSchedulerBehavior::add(AbstractList *l)
{
  queue->add(l);
}

AbstractQueue *AbstractSchedulerBehavior::getQueue ()
{
  return queue;
}

int AbstractSchedulerBehavior::getIterations()
{
  return iterations;
}
