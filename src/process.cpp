#include "process.hpp"

Process::Process (int pid, int start) : pid(pid), time(start)
{
  state = INVALID;
  tasks = new TaskQueue();
  elapsed = 0;
  remainingTime = 0;
}

void Process::addTask (bool IO, int duration)
{
  Task *t = new Task(IO, duration);
  totalTime += duration;
  remainingTime += duration;
  tasks->addTask(t);
}

//time is the iterations spent in queue
//duration is the max allowed running iterations
ProcessExecuteData *Process::execute (int time, int duration)
{
  ProcessExecuteData *ped = new ProcessExecuteData(pid);
/*
  if (duration == 0) {
    state = PAUSED;
    return 0;
  }
*/
  state = RUNNING;
  elapsed += time;
  bool done = false;
  int time_used;

  while (!done) {
    if (tasks->isEmpty()) {
      state = STOPPED;
      return ped;
    }
    TaskList *next = (TaskList*)tasks->getFirst()->getNext();
    if (tasks->getTask()->needsCPU()) {
      time_used = tasks->getTask()->iterate(duration);
      remainingTime -= time_used;
      elapsed += time_used;
      ped->addElement(time_used, RUNNING);
      time = 0;
      if (next->getTask()->needsCPU())
        duration -= time_used;
      else
        duration = 0;
    }
    else {
      if (next->getTask()->needsCPU() && tasks->getTask()->getRemaining() < time + duration) {
        ped->addElement(tasks->getTask()->getRemaining(), IO);
        remainingTime -= tasks->getTask()->iterate(time);
        remainingTime -= (time_used = tasks->getTask()->iterate(duration));
        duration -= time_used;
        time = 0;
      }
      else {
        remainingTime -= (time_used = tasks->getTask()->iterate(time));
        time -= time_used;
        ped->addElement(time_used, IO);
        if (next->getTask()->needsCPU())
          ped->addElement(time, PAUSED);
      }
    }
    if (tasks->getTask()->getRemaining() == 0) {
      tasks->removeFirst();
    }
    if (duration <= 0) {
      done = true;
    }
  }

  return ped;
}

State Process::getState ()
{
  return state;
}

void Process::setState (State s)
{
  state = s;
}

int Process::getPID ()
{
  return pid;
}

int Process::getElapsed()
{
  return elapsed;
}

int Process::getTotalTime()
{
  return totalTime;
}

int Process::getRemaingTime()
{
  return remainingTime;
}
