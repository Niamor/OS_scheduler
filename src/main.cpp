#include "FIFOBehavior.hpp"
#include "RRBehavior.hpp"
#include "scheduler.hpp"
#include "main.hpp"

ProcessQueue* c(){
  Process *process;
  ProcessQueue *queue = new ProcessQueue();

  process = new Process(6, 3);
  process->addTask(false, 5);
  process->addTask(true, 3);
  process->addTask(false, 2);
  queue->addProcess(process, 0);

  process = new Process(7, 10);
  process->addTask(false, 5);
  process->addTask(true, 3);
  process->addTask(false, 2);
  queue->addProcess(process, 0);

  process = new Process(8, 10);
  process->addTask(false, 5);
  process->addTask(true, 3);
  process->addTask(false, 2);
  queue->addProcess(process, 0);

  process = new Process(9, 10);
  process->addTask(false, 5);
  process->addTask(true, 3);
  process->addTask(false, 2);
  queue->addProcess(process, 0);

  process = new Process(10, 15);
  process->addTask(false, 5);
  process->addTask(true, 3);
  process->addTask(false, 2);
  queue->addProcess(process, 0);
  return queue;
}
ProcessQueue* b(){
  Process *process;
  ProcessQueue *queue = new ProcessQueue();

  process = new Process(1, 0);
  process->addTask(false, 5);
  process->addTask(true, 3);
  process->addTask(false, 2);
  queue->addProcess(process, 0);

  process = new Process(2, 0);
  process->addTask(false, 5);
  process->addTask(true, 3);
  process->addTask(false, 2);
  queue->addProcess(process, 0);

  process = new Process(3, 0);
  process->addTask(false, 5);
  process->addTask(true, 3);
  process->addTask(false, 2);
  queue->addProcess(process, 0);

  process = new Process(4, 0);
  process->addTask(false, 5);
  process->addTask(true, 3);
  process->addTask(false, 2);
  queue->addProcess(process, 0);

  process = new Process(5, 0);
  process->addTask(false, 5);
  process->addTask(true, 3);
  process->addTask(false, 2);
  queue->addProcess(process, 0);
  return queue;
}

int main(int argc, char const **argv) {
/*
    Parser *parser = new Parser();

    ProcessQueue** queue = parser->parse("sample.xml");
*/
  Scheduler scheduler;
  ProcessQueue **queues = (ProcessQueue**)malloc(2 * sizeof(ProcessQueue*));
  int tmp;

  SchedulerBehavior *behavior = new FIFOBehavior();
  scheduler.setCurrentBehavior((FIFOBehavior*) behavior);
  queues[0] = b();
  queues[1] = c();
  scheduler.init(queues);
  do {
    tmp = scheduler.processes();
    printf("\ntmp = %d\n\n", tmp);
  } while (tmp != -1);


  behavior = new RRBehavior(5);
  scheduler.setCurrentBehavior((RRBehavior*) behavior);
  queues[0] = b();
  queues[1] = c();
  scheduler.init(queues);
  do {
    tmp = scheduler.processes();
    printf("\ntmp = %d\n\n", tmp);
  } while (tmp != -1);

  return 0;
}
