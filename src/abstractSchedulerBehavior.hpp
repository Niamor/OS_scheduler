#ifndef ABSTRACT_SCHEDULER_BEHAVIOR_HPP
#define ABSTRACT_SCHEDULER_BEHAVIOR_HPP

#include "abstractQueue.hpp"
#include "schedulerBehavior.hpp"

class AbstractSchedulerBehavior : public SchedulerBehavior
{
protected:
  AbstractQueue *queue;
  int iterations;

  AbstractSchedulerBehavior();
  AbstractSchedulerBehavior(AbstractQueue *q);
  AbstractQueue *getQueue();

public:
  int getIterations();
  void add(AbstractList *l);
  virtual int routine() = 0;
  void setQueue(AbstractQueue *q);

};

#endif
