#ifndef PRIORITY_PROCESS_QUEUE_HPP
#define PRIORITY_PROCESS_QUEUE_HPP

#include "processQueue.hpp"

class PriorityProcessQueue : public ProcessQueue
{
private:
  typedef ProcessQueue super;

public:
  PriorityProcessQueue();
  ProcessList *pop();
};

#endif
