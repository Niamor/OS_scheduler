#ifndef TASK_HPP
#define TASK_HPP

class Task
{
private:
  const bool IO;
  const int duration;
  int elapsed;

public:
  Task(bool IO, int d);
  bool needsCPU();
  int getRemaining();
  int getDuration();
  int getElapsed();
  int iterate(int i);
};

#endif
