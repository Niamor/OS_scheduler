#ifndef RR_BEHAVIOR_HPP
#define RR_BEHAVIOR_HPP

#include "logger.hpp"
#include "processQueue.hpp"
#include "abstractSchedulerBehavior.hpp"

class RRBehavior : public AbstractSchedulerBehavior
{
private:
  const int quantum;
  typedef AbstractSchedulerBehavior super;

public:
  RRBehavior(int qt);
  RRBehavior(ProcessQueue *q, int qt);
  void addProcess(Process *p);
  int routine();
};

#endif
