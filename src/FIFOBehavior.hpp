#ifndef FIFO_BEHAVIOR_HPP
#define FIFO_BEHAVIOR_HPP

#define       FIFO_RATE     99

#include "logger.hpp"
#include "processQueue.hpp"
#include "abstractSchedulerBehavior.hpp"

class FIFOBehavior : public AbstractSchedulerBehavior
{
private:
  typedef AbstractSchedulerBehavior super;

public:
  FIFOBehavior();
  FIFOBehavior(ProcessQueue *q);
  void addProcess(Process *p);
  int routine();
};

#endif
