#ifndef SCHEDULER_DATA_HPP
#define SCHEDULER_DATA_HPP

#include "processExecuteData.hpp"

class SchedulerData
{
private:

public:
  std::vector<ProcessExecuteData> peds;

};

#endif
