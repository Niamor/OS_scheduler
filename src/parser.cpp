#include "parser.hpp"

Parser::Parser(){
    XMLPlatformUtils::Initialize();
    xmlParser = new XercesDOMParser;
}

ProcessQueue** Parser::parse(string filename)
{
    printf("File testing\n");

    struct stat filestatus;

    if(stat(filename.c_str(), &filestatus) == -1){
        printf("Error access to file\n");
        exit(1);
    }

    printf("Initializing parser\n");

    xmlParser->setValidationScheme( XercesDOMParser::Val_Never );
    xmlParser->setDoNamespaces( false );
    xmlParser->setDoSchema( false );
    xmlParser->setLoadExternalDTD( false );

    printf("Beginning to parse\n");
    xmlParser->parse(filename.c_str());

    printf("File parsed\n");

    ProcessQueue *queue1 = new ProcessQueue();
    ProcessQueue *queue2 = new ProcessQueue();

    DOMDocument* xmlDoc = xmlParser->getDocument();
    DOMElement* rootElement = xmlDoc->getDocumentElement();

    DOMNodeList* processes = rootElement->getChildNodes();

    const XMLSize_t processCount = processes->getLength();

    Process *p;

    for(XMLSize_t i = 0; i < processCount; i++)
    {
        DOMNode* process = processes->item(i);
        if(process->getNodeType() && process->getNodeType() == DOMNode::ELEMENT_NODE)
        {
            DOMElement *processElement = dynamic_cast<DOMElement*>(process);
            int date =  atoi(
                XMLString::transcode(
                    processElement->getAttribute(
                        XMLString::transcode("date")
                    )
                )
            );
            p = new Process(i, date);

            DOMNodeList* tasks = processElement->getChildNodes();

            const XMLSize_t taskCount = tasks->getLength();

            for(XMLSize_t j = 0; j < taskCount; j++)
            {
                DOMNode* task = tasks->item(j);
                if(task->getNodeType() && task->getNodeType() == DOMNode::ELEMENT_NODE)
                {
                    DOMElement *taskElement = dynamic_cast<DOMElement*>(task);

                    int time = atoi(XMLString::transcode(taskElement->getAttribute(XMLString::transcode("time"))));
                    if(XMLString::equals(taskElement->getAttribute(XMLString::transcode("type")), XMLString::transcode("IO"))){
                        p->addTask(true, time);
                    } else {
                        p->addTask(false, time);
                    }
                }
            }
            if(date == 0){
                queue1->addProcess(p, date);
            } else {
                queue2->addProcess(p, date);
            }
        }
    }

    ProcessQueue** queues = (ProcessQueue**) malloc(2*sizeof(ProcessQueue*));
    queues[0] = queue1;
    queues[1] = queue2;
    return queues;
}
