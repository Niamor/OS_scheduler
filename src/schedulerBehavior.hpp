#ifndef SCHEDULERBEHAVIOR
#define SCHEDULERBEHAVIOR

#include "processExecuteData.hpp"

class SchedulerBehavior {
public:
  virtual ProcessExecuteData *routine() = 0;
};

#endif // SCHEDULERBEHAVIOR
