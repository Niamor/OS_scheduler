#ifndef TASK_LIST_HPP
#define TASK_LIST_HPP

#include "abstractList.hpp"
#include "task.hpp"

class TaskList : public AbstractList
{
private:
  typedef AbstractList super;

public:
  TaskList(Task *t);
  Task *getTask();
  TaskList *getNext();
  void setNext(TaskList *l);
};


#endif
