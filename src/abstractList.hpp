#ifndef ABSTRACT_LIST_HPP
#define ABSTRACT_LIST_HPP
#include <stdio.h>

class AbstractQueue;

class AbstractList
{
public:
  bool isEmpty();
  AbstractList *getNext();

protected:
  void *value;
  AbstractList *next;

  AbstractList(void *v);
  void *getValue();
  void setNext(AbstractList *l);

  friend AbstractQueue;
};

#endif
