#include "processExecuteData.hpp"

ProcessExecuteData::ProcessExecuteData (int p) : pid(p)
{
  totalTime = 0;
}

void ProcessExecuteData::addElement (int time, State s)
{
  if (time > 0) {
    DataElement* tmp = new DataElement(time, s);
    tasks.push_back(tmp);
    totalTime += time;
  }
}

int ProcessExecuteData::getPID()
{
  return pid;
}
int ProcessExecuteData::getTotalTime()
{
  return totalTime;
}
