#ifndef ABSTRACT_QUEUE_HPP
#define ABSTRACT_QUEUE_HPP

#include <stdio.h>
#include "abstractList.hpp"

class AbstractQueue
{
protected:
  AbstractList *head, *tail;
  int size;

public:
  AbstractQueue();
  void add(AbstractList *l);
  void addElement(void *v);
  void removeFirst();
  AbstractList *getFirst();
  AbstractList *pop();
  int isEmpty();
  int getSize();
};

#endif
