#ifndef ABSTRACT_PREEMPTIVE_SCHEDULER_BEHAVIOR_HPP
#define ABSTRACT_PREEMPTIVE_SCHEDULER_BEHAVIOR_HPP

#include "abstractSchedulerBehavior.hpp"

class AbstractPreemptiveSchedulerBehavior : public AbstractSchedulerBehavior
{
private:
  typedef AbstractSchedulerBehavior super;

protected:
  AbstractPreemptiveSchedulerBehavior(AbstractQueue *q);

public:
  virtual ProcessExecuteData *routine() = 0;
  void interrupt();
};

#endif
