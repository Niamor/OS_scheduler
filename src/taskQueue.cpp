#include "taskQueue.hpp"

TaskQueue::TaskQueue () : super()
{
}

void TaskQueue::addTask (Task *t)
{
  super::addElement((void*)t);
}

Task *TaskQueue::getTask ()
{
  return ((TaskList*)super::getFirst())->getTask();
}
