#include "SJFBehavior.hpp"

SJFBehavior::SJFBehavior () : super()
{
  queue = new PriorityProcessQueue();
}

SJFBehavior::SJFBehavior (ProcessQueue *q) : super((AbstractQueue*)q)
{
}

void SJFBehavior::addProcess (Process *p)
{
  ((ProcessQueue*)queue)->addProcess(p, iterations);
}

ProcessExecuteData *SJFBehavior::routine ()
{
  puts("test");
  ProcessExecuteData *ped = NULL;
  if (queue->isEmpty()) {
    return ped;
  }
  ProcessList *tmp = (ProcessList*)((PriorityProcessQueue*)queue)->pop();
  char buffer[BUF_SIZE];
  sprintf(buffer, "PID %d : Duration %d : State %d\n", tmp->getProcess()->getPID(), tmp->getProcess()->getElapsed(), tmp->getProcess()->getState());
  puts(buffer);
  ped = tmp->getProcess()->execute(iterations - tmp->getDate(), FIFO_RATE);

  iterations += ped->getTotalTime();

  sprintf(buffer, "PID %d : Duration %d : State %d\n", tmp->getProcess()->getPID(), tmp->getProcess()->getElapsed(), tmp->getProcess()->getState());
  puts(buffer);

  if (tmp->getProcess()->getState() != STOPPED) {
    puts("back in queue");
    tmp->setDate (iterations);
    queue->add(tmp);
  }
  else {
    puts("stopped");
    delete(tmp);
  }

  return ped;
}
