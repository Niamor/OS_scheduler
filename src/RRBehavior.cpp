#include "RRBehavior.hpp"

RRBehavior::RRBehavior (ProcessQueue *q, int qt) : super((AbstractQueue*)q), quantum(qt)
{
}

RRBehavior::RRBehavior (int qt) : super(), quantum(qt)
{
}

void RRBehavior::addProcess (Process *p)
{
  ((ProcessQueue*)queue)->addProcess(p, iterations);
}

int RRBehavior::routine ()
{
  puts("test");
  ProcessList *tmp = (ProcessList*)queue->pop();
  char buffer[BUF_SIZE];
  sprintf(buffer, "PID %d : Duration %d : State %d\n", tmp->getProcess()->getPID(), tmp->getProcess()->getElapsed(), tmp->getProcess()->getState());
  puts(buffer);

  ProcessExecuteData *ped = tmp->getProcess()->execute(iterations - tmp->getDate(), quantum);

  iterations += ped->getTotalTime();

  sprintf(buffer, "PID %d : Duration %d : State %d\n", tmp->getProcess()->getPID(), tmp->getProcess()->getElapsed(), tmp->getProcess()->getState());
  puts(buffer);
//  Logger::getInstance().log(LOG_INFO, buffer);

  if (tmp->getProcess()->getState() != STOPPED) {
    puts("back in queue");
    tmp->setDate (iterations);
    queue->add(tmp);
  }
  else {
    puts("stopped");
    delete(tmp);
  }

  if (queue->isEmpty()) {
    puts("empty queue");
    return -1;
  }

  return 0;
}
