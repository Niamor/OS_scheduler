#include "task.hpp"

Task::Task (bool IO, int d) : IO(IO), duration(d)
{
  elapsed = 0;
}

bool Task::needsCPU ()
{
  return !IO;
}

int Task::getRemaining ()
{
  return duration - elapsed;
}

int Task::getDuration ()
{
  return duration;
}

int Task::getElapsed ()
{
  return elapsed;
}

int Task::iterate (int i)
{
  int remaining = duration - elapsed;
  if (i >= remaining) {
    elapsed = duration;
    return remaining;
  }
  elapsed += i;
  return i;
}
