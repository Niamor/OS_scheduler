#include "logger.hpp"

Logger Logger::INSTANCE = Logger();

Logger::Logger()
{
  open("logs/test.dat");
  log(LOG_TRACE, "Logger started\n");
}

Logger::~Logger()
{
  log(LOG_TRACE, "Logger closed\n");
  close();
}

Logger& Logger::getInstance()
{
  return INSTANCE;
}

void Logger::open(const char *name)
{
  output.open(name, std::ios::out | std::ios::app);
}

void Logger::close()
{
  output.close();
}

void Logger::write(const std::string message)
{
  output << message << std::endl;
}

void Logger::log(Dev_Log_Level level, const std::string message)
{
  /*
  std::string str;
  str.append(levelToString(level));
  str.append(message);
  write(str);
  */
  write('[' + levelToString(level) + "] " + message);
}

std::string Logger_policy::levelToString(Dev_Log_Level level) const
{
  switch (level) {
    case 0:
      return "Log debug";
    case 1:
      return "Log trace";
    case 2:
      return "Log info";
    case 3:
      return "Log warning";
    case 4:
      return "Log error";
  }
}
