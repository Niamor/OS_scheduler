#ifndef TASK_QUEUE_HPP
#define TASK_QUEUE_HPP

#include "abstractQueue.hpp"
#include "taskList.hpp"

class TaskQueue : public AbstractQueue
{
private:
  typedef AbstractQueue super;

public:
  TaskQueue();
  void addTask(Task *t);
  Task *getTask();
};

#endif
