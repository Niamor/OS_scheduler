#include "abstractQueue.hpp"

AbstractQueue::AbstractQueue ()
{
  head = NULL;
  tail = NULL;
  size = 0;
}

void AbstractQueue::add (AbstractList *l)
{
  if (head == NULL) {
    head = tail = l;
  }
  else {
    tail->setNext(l);
    tail = l;
  }
  size++;
}

void AbstractQueue::addElement (void *v)
{
  AbstractList *tmp = new AbstractList(v);
  add(tmp);
}

AbstractList *AbstractQueue::pop ()
{
  AbstractList *tmp = head;
  if (head != NULL)
    head = head->getNext();
  if (head == NULL)
    tail = NULL;
  size--;
  return tmp;
}

void AbstractQueue::removeFirst ()
{
  delete(pop());
}

AbstractList *AbstractQueue::getFirst ()
{
  return head;
}

int AbstractQueue::isEmpty ()
{
  return head == NULL || size <= 0;
}

int AbstractQueue::getSize ()
{
  return size;
}
