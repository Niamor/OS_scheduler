#ifndef SCHEDULER_H
#define SCHEDULER_H

#include "abstractSchedulerBehavior.hpp"
#include "processQueue.hpp"
#include "taskList.hpp"

class Scheduler
{
public:
  Scheduler();
  void init(ProcessQueue **q);
  int processes();
  void setCurrentBehavior(AbstractSchedulerBehavior *behavior);

private:
  ProcessQueue *queue;
  AbstractSchedulerBehavior *currentBehavior;
};

#endif // SCHEDULER_H
