#ifndef PROCESS_EXECUTE_DATA_HPP
#define PROCESS_EXECUTE_DATA_HPP

#include <vector>

enum State
{
  INVALID = -1,
  READY = 0,
  RUNNING = 1,
  IO = 2,
  PAUSED = 2,
  STOPPED = 3
};

class DataElement
{
public:
  int time;
  State state;
  DataElement (int t, State s) : time(t), state(s){}
};

class ProcessExecuteData
{
private:
  std::vector<DataElement*> tasks;
  int pid, totalTime;

public:
  ProcessExecuteData(int p);
  int getPID();
  int getTotalTime();
  void addElement(int time, State s);
};

#endif
