# Installation and User manual

Authors :
- Romain Gourlat
- David de Castilla

This project is published under Beer License (see [LICENSE.md](LICENSE.md))

## Scheduler project
This program allows you to test differents politics of schedulers :
- FIFO
- SRJF
- SJF
- RR

### Installation
#### Xerces library
For this project, we use the xerces library in order to parse the XML configuration file. So you have to install this library first. If the library is already installed on your computer, you can skip this step.
##### Installation from apt-get repository
Open your terminal and execute the command :
```
sudo apt-get install libxerces-c-dev
```
Once the execution finished, the library is installed and you can go to the software installation part.

##### Installation with downloading and compiling the sources

- You have to start to download the sources at this URL :
[http://xerces.apache.org/xerces-c/download.cgi](http://xerces.apache.org/xerces-c/download.cgi)
- Download the compressed file in the format you want.
- Uncompress the file and go to the sources directory with a terminal.
- Execute the command :
```
./configure
```
on your terminal.
- Then install the library with the command :
```
sudo make install
```
- After the execution of this command, the library is installed.
- You can find more informations about this installation way at this URLs :
[http://xerces.apache.org/xerces-c/install-3.html](http://xerces.apache.org/xerces-c/install-3.html)

#### Software installation
- Go to the sources directory of our software with your terminal and execute the command :
```
make install
```
### Utilisation
#### Configuration file
The configuration is the file where you specify the processes. It has an XML format.
Here’s an example of this configuration file :
```
<?xml version="1.0" encoding="UTF-8"?>
<processes>
    <process date="0">
        <task type="CPU" time="4"/>
        <task type="IO" time="12"/>
        <task type="CPU" time="2"/>
        <task type="IO" time="3"/>
        <task type="CPU" time="5"/>
        <task type="IO" time="1"/>
    </process>
    <process date="1">
        <task type="CPU" time="5"/>
        <task type="IO" time="1"/>
        <task type="CPU" time="15"/>
        <task type="IO" time="2"/>
        <task type="CPU" time="3"/>
        <task type="IO" time="4"/>
        <task type="CPU" time="1"/>
    </process>
    <process date="1">
        <task type="CPU" time="1"/>
        <task type="IO" time="2"/>
        <task type="CPU" time="3"/>
        <task type="IO" time="4"/>
        <task type="CPU" time="5"/>
        <task type="IO" time="6"/>
        <task type="CPU" time="7"/>
    </process>
</processes>
```

- The `<processes>` tag contains all the processes, they are specified by the `<process>` tag. The process have to be in the croissant order of the date.
- The `<process>` tag has one attribute :
    - `date` : in this attribute, you specify when the process arrive in the scheduler, 0 is for the first iteration, 1 for the second, etc…
- The `<process>` tag contains the tasks of the process in the order of execution. They are specified by the `<task>` tag.
- The `<task>` tag has two attributes :
    - `type` : this attribute have two possible values :
        - `CPU` to specify that the process needs the CPU.
        - `IO` to specify that the process needs to do IO operation
    - `time` : to specify for how much time he needs to the CPU or to do an IO operation

#### Running the software
- To run the program without options, you have to open a terminal and go to the sources directory, then execute the command :
```
make Run
```

- To run the software with options, you have to open a terminal and go to the sources directory, then execute the program with the command :
```
./project/bin/main -b [scheduler you want] -f [configuration file path]
```
Options :
- `-b [scheduler]` : If specified, the scheduler is the behavior you want the scheduler to run, that can be :
    - FIFO
    - SJF
    - SRJF
    - RR
- `-f [configuration file path]` : If specified the program will use the configure file you type as [configuration file path]. If not specified the program will ask you to enter the path to your configuration file.

#### Outputs
The program outputs a csv file with informations about the execution. You can open this csv file with a spreadsheet editor like Microsoft Excel or LibreOffice Calc.
