CC = g++
CFLAGS =-ggdb -lxerces-c
SOURCEDIR =src
HEADERDIR =src
LOGSDIR =logs
BINDIR =bin
BUILDDIR =build
SOURCES =$(wildcard $(SOURCEDIR)/*.cpp)
OBJECTS =$(patsubst $(SOURCEDIR)/%.cpp, $(BUILDDIR)/%.o, $(SOURCES))
BINARY = main

all: $(BINARY)

$(BINARY): $(BUILDDIR)/$(OBJECTS)
	$(CC) $(CFLAGS)$(OBJECTS) -o $(BINDIR)/$@

$(BUILDDIR)/%.o: $(SOURCEDIR)/%.cpp $(HEADERDIR)/%.hpp
	$(CC) $(CFLAGS) -c $< -o $@

clean_all: clean clean_logs
	rm -f $(BINDIR)/*
clean:
	rm -f $(BUILDDIR)/*.o
clean_logs:
	rm $(LOGSDIR)/*.dat
